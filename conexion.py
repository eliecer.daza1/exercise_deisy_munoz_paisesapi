import requests
import sqlite3
from sqlite3 import Error

class ApiConexion:

    def ApiRegiones(self):

        """
        Se realiza la solicitud para tomar la api de la informacion de todos los paises
        
        Se devuelve una lista con la información de todos los paises
        """

        url1 = "https://restcountries-v1.p.rapidapi.com/all"

        headers = {
        'x-rapidapi-host': "restcountries-v1.p.rapidapi.com",
        'x-rapidapi-key': "52f2616fb8msh9a4d80cbc93f771p188e20jsn2f83b37b7a49"
        }

        response =requests.request("GET", url1, headers=headers)
        data= response.json()
        return data

    def ApiPaisesporRegion(self,region):
        """
        Se realiza la solicitud para tomar los paises por región

        Se devuelve una lista con la informacion de paises por región

        Parametro
        region= deficion de la region para devolver correctamente la lista de paises por región 
        """

        url="https://restcountries.eu/rest/v2/region/"
        data =requests.get(url+region).json()

        return data

    def ConexionSqlite(self):
        """
        Se crea la conexion a la base de datos incluyendo la creacion de la tabla para almacenar la informacion de los paises
        Se retorna la conexion de la bd
        """

        db = r"dbsqlite.db"
        conn = None
        try:
            conn = sqlite3.connect(db)
        except Error as e:
            print(e)
        cursorObj = conn.cursor()

        cursorObj.execute("CREATE TABLE Paises(id integer PRIMARY KEY, region text, pais text, lenguaje text, time real)")
        
        conn.commit() 
        
        # return cur.lastrowid
        return conn


    






